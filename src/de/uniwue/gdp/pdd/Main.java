package de.uniwue.gdp.pdd;


import de.uniwue.gdp.pdd.deck.Card;
import de.uniwue.gdp.pdd.deck.Deck;
import de.uniwue.gdp.pdd.deck.Standard52;
import de.uniwue.gdp.pdd.deck.Suit;
import de.uniwue.gdp.pdd.rules.Rules;

import java.util.ArrayList;
import java.util.List;

public class Main {
/*    public static void main(String[] args) {
        Card c1 = new Card(Suit.HEARTS, 4);
        System.out.println( c1.toString() );
        System.out.println( c1.hashCode() );
        Card c2 = new Card(Suit.HEARTS, 4);
        System.out.println( c2.equals(c1) );

        Deck deck = Standard52.singleDeck();
        deck.play( c2 );
        System.out.println( deck.take() );
        System.out.println( deck.top() );

        System.out.println( Rules.sameValueRule().test(c1, c2) );
        Card c3 = new Card(Suit.HEARTS, 3);
        System.out.println( Rules.sameValueRule().test(c1, c3) );

        Card c4 = new Card(Suit.DIAMONDS, 3);

        CardsPair cardsPair1 = new CardsPair(c3,c4);
        CardsPair cardsPair2 = new CardsPair(c3,c4);
        System.out.println( cardsPair1 );
        System.out.println( cardsPair2 );

        List<CardsPair> cardsPairs = new ArrayList<>();
        cardsPairs.add(cardsPair1);

        if( cardsPair1.equals(cardsPair2) ) {
            System.out.println("card pairs are equal!");
        }
        if( cardsPairs.contains(cardsPair2) ) {
            System.out.println("contained!");
        }
    }*/

    public static void main(String[] args) {
        Deck deck = Standard52.doubleDeck();
        List<Player> players = new ArrayList<>();
        players.add(new SimplePlayer("Hans"));
        players.add(
                new SimplePlayer(
                        "Mara",
                        Rules.sameSuitRule()
                )
        );
        Game game = new Game(players, deck);
        game.start();
        System.out.println(game.winner());
    }
}