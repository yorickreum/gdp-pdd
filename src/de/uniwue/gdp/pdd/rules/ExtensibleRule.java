package de.uniwue.gdp.pdd.rules;

import de.uniwue.gdp.pdd.deck.Card;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

public class ExtensibleRule implements BiPredicate<Card, Card> {
    private BiPredicate<Card, Card> baseRule;

    private List<CardsPair> allowedPairs;
    private List<CardsPair> forbiddenPairs;

    public ExtensibleRule(BiPredicate<Card, Card> base) {
        this.baseRule = base;
        this.allowedPairs = new ArrayList<>();
        this.forbiddenPairs = new ArrayList<>();
    }

    public BiPredicate<Card, Card> getBaseRule() {
        return this.baseRule;
    }

    public void addAllowed(Card top, Card played) {
        CardsPair cardsPair = new CardsPair(top, played);
        this.allowedPairs.add( cardsPair );
    }

    public void addForbidden(Card top, Card played) {
        CardsPair cardsPair = new CardsPair(top, played);
        this.forbiddenPairs.add( cardsPair );
    }

    @Override
    public boolean test(Card top, Card played) {
        CardsPair playedPair = new CardsPair(top, played);
        return baseRule.test(top, played) && !forbiddenPairs.contains(playedPair);
    }

    public boolean testAllowed(Card top, Card played) {
        CardsPair playedPair = new CardsPair(top, played);
        return baseRule.test(top, played) && allowedPairs.contains(playedPair);
    }

    public boolean isCardPairUnknown(Card top, Card played) {
        CardsPair playedPair = new CardsPair(top, played);
        return !allowedPairs.contains(playedPair) && !forbiddenPairs.contains(playedPair);
    }

}