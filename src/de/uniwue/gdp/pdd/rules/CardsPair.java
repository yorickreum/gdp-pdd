package de.uniwue.gdp.pdd.rules;

import de.uniwue.gdp.pdd.deck.Card;

public class CardsPair {
    private Card top;
    private Card played;

    public CardsPair(Card top, Card played) {
        this.top = top;
        this.played = played;
    }

    public Card getTop() {
        return top;
    }

    public Card getPlayed() {
        return played;
    }

    @Override
    public String toString() {
        return "top: '" + top.toString() + "' played: '" + this.played.toString() + "'";
    }

    @Override
    public int hashCode() {
        String concat = "" + this.top.hashCode() + this.played.hashCode();
        return Integer.parseInt(concat);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof CardsPair)) return false;
        CardsPair otherCardsPair = (CardsPair) other;
        if (otherCardsPair.getTop() != this.top) return false;
        if (otherCardsPair.getPlayed() != this.played) return false;
        return true;
    }
}
