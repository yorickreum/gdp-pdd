package de.uniwue.gdp.pdd.rules;

import de.uniwue.gdp.pdd.deck.Card;

import java.util.function.BiPredicate;

public class Rules {
    public static BiPredicate<Card, Card> sameValueRule() {
        return (top, played) -> (top.value() == played.value());
    }

    public static BiPredicate<Card, Card> sameSuitRule() {
        return (top, played) -> (top.suit() == played.suit());
    }

    public static BiPredicate<Card, Card> increasingValueRule() {
        return (top, played) -> (top.value() < played.value());
    }

    public static BiPredicate<Card, Card> jokerRule(Card joker) {
        return (top, played) -> ( played.equals(joker) );
    }
}
