package de.uniwue.gdp.pdd;

import de.uniwue.gdp.pdd.deck.Card;
import de.uniwue.gdp.pdd.deck.Suit;
import de.uniwue.gdp.pdd.rules.ExtensibleRule;
import de.uniwue.gdp.pdd.rules.Rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;

public class SimplePlayer extends Player {

    private ExtensibleRule extensibleRule;

    public static BiPredicate<Card, Card> mauMauRule() {
        return (top, played) -> (Rules.sameValueRule().test(top, played) || Rules.sameSuitRule().test(top, played));
    }

    public static BiPredicate<Card, Card> jokersRule() {
//        Suit[] suits = {Suit.DIAMONDS, Suit.CLUBS, Suit.HEARTS, Suit.SPADES};
//        for (Suit suit : suits) {
//            Card card = new Card(suit, 11); // 11 = Bube
//            ExtensibleRule extensibleRule = new ExtensibleRule(Rules.jokerRule(card));
//            extensibleRules.add(extensibleRule);
//        }

        BiPredicate<Card, Card> diamondsJoker = Rules.jokerRule(new Card(Suit.DIAMONDS, 11));
        BiPredicate<Card, Card> clubsJoker = Rules.jokerRule(new Card(Suit.CLUBS, 11));
        BiPredicate<Card, Card> heartsJoker = Rules.jokerRule(new Card(Suit.HEARTS, 11));
        BiPredicate<Card, Card> spadesJoker = Rules.jokerRule(new Card(Suit.SPADES, 11));

        return (top, played) -> (
                diamondsJoker.test(top, played)
                        || clubsJoker.test(top, played)
                        || heartsJoker.test(top, played)
                        || spadesJoker.test(top, played)
        );
    }

    public static BiPredicate<Card, Card> mauMauJokerRule() {
        return (top, played) -> (jokersRule().test(top, played) || mauMauRule().test(top, played));
    }

    public SimplePlayer(String name) {
        super(name);
        this.extensibleRule = new ExtensibleRule(mauMauJokerRule());
    }

    public SimplePlayer(String name, BiPredicate<Card, Card> rule) {
        super(name);
        this.extensibleRule = new ExtensibleRule(rule);
    }

    @Override
    public void validMove(Player player, Card top, Card played) {
        this.extensibleRule.addAllowed(top, played);
    }

    @Override
    public void invalidMove(Player player, Card top, Card played) {
        this.extensibleRule.addForbidden(top, played);
    }

    @Override
    public BiPredicate<Card, Card> invent() {
        return this.extensibleRule.getBaseRule(); // or just our extended rule?
    }

    @Override
    public Optional<Card> play(Card top) {

        List<Card> allowedHandCards = new ArrayList<>();
        for (Card handCard : this.hand) { // check if any card suits our own rules
            if (extensibleRule.getBaseRule().test(top, handCard)) {
                allowedHandCards.add(handCard);
            }
        }

        if (allowedHandCards.isEmpty()) {
            return Optional.empty();
        }

        for ( Card allowedHandCard: allowedHandCards) {
            if (extensibleRule.testAllowed(top, allowedHandCard)) {
                this.hand.remove(allowedHandCard);
                return Optional.of(allowedHandCard);
            }
        }
        for ( Card allowedHandCard: allowedHandCards) {
            if (extensibleRule.isCardPairUnknown(top, allowedHandCard)) {
                this.hand.remove(allowedHandCard);
                return Optional.of(allowedHandCard); // should be unknown
            }
        }

        Card cardToPlay = allowedHandCards.get(0);
        this.hand.remove(cardToPlay);
        return Optional.of( cardToPlay  ); // should be forbidden

    }
}
