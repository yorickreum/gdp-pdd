package de.uniwue.gdp.pdd;

import de.uniwue.gdp.pdd.deck.Card;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;

public abstract class Player implements GameListener {

    protected final String name;
    protected List<Card> hand;

    protected Player(String name) {
        this.name = name;
        this.hand = new ArrayList<>();
    }

    public void startNewGame(Collection<Card> hand) {
        this.hand = new ArrayList<>(hand);
    }

    public void take(Card card) {
        this.hand.add(card);
    }

    public int numberOfCards() {
        return this.hand.size();
    }

    public String name() {
        return this.name;
    }

    public abstract BiPredicate<Card, Card> invent();

    public abstract Optional<Card> play(Card top);

    @Override
    public String toString() {
        return this.name;
    }
}
