package de.uniwue.gdp.pdd;

import de.uniwue.gdp.pdd.deck.Card;
import de.uniwue.gdp.pdd.deck.Deck;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Game {
    private List<Player> players;
    private Deck deck;

    private Optional<Player> winner;

    public Game(List<Player> players, Deck deck) {
        this.players = players;
        this.deck = deck;
        this.winner = Optional.empty();
    }

    public void start() {
        for (Player player : this.players) {
            List<Card> hand = new ArrayList<>();
            for (int i = 0; i < 7; i++) {
                hand.add(deck.take());
            }
            player.startNewGame(hand);
        }

        while (!this.winner.isPresent()) { // unnecessary because breaking by return
            for (Player player : this.players) {
                Card top = deck.top();
                Optional<Card> cardToPlace = player.play(top);
                if (!cardToPlace.isPresent()) {
                    player.take(deck.take());
                } else {
                    Card playedCard = cardToPlace.get();
                    boolean validMove = true;
                    for (Player playerToCheck : players) {
                        if (!playerToCheck.invent().test(top, playedCard)) {
                            validMove = false;
                            if (playerToCheck.equals(player)) {
                                throw new IllegalMoveException("Card didn't suit own rules!");
                            }
                        }
                    }
                    if (player.numberOfCards() == 0) {
                        this.winner = Optional.of(player);
                        return;
                    }
                    if (!validMove) {
                        player.take(deck.take());
                    }
                    for (Player playerToInform : players) {
                        if (validMove) {
                            playerToInform.validMove(player, top, playedCard);
                        } else {
                            playerToInform.invalidMove(player, top, playedCard);
                        }
                    }
                    deck.play(playedCard);
                }
            }
        }
    }


    public Optional<Player> winner() {
        return this.winner;
    }
}
