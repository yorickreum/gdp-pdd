package de.uniwue.gdp.pdd;

import de.uniwue.gdp.pdd.deck.Card;

public interface GameListener {
    void validMove(Player player, Card top, Card played);
    void invalidMove(Player player, Card top, Card played);
}
