package de.uniwue.gdp.pdd.deck;

public enum Suit {
    DIAMONDS(1),
    CLUBS(2),
    HEARTS(3),
    SPADES(4);

    private final int value;
    private Suit(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
