package de.uniwue.gdp.pdd.deck;

public interface Deck {
    Card take();
    void play(Card card);
    Card top();
}
