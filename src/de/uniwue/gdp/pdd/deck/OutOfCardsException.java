package de.uniwue.gdp.pdd.deck;

public class OutOfCardsException extends RuntimeException {
    // some completly useless constructors

    public OutOfCardsException() {
        super();
    }

    public OutOfCardsException(String message) {
        super(message);
    }

    public OutOfCardsException(String message, Throwable cause) {
        super(message, cause);
    }

    public OutOfCardsException(Throwable cause) {
        super(cause);
    }
}
