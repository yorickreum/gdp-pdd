package de.uniwue.gdp.pdd.deck;

public class Card {
    private Suit suit;
    private int value; //  Ass = 1, Bube = 11, Dame = 12 und König = 13.

    public Card(Suit suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public Suit suit() {
        return this.suit;
    }

    public int value() {
        return this.value;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Card)) return false;
        Card otherCard = (Card) other;
        if (otherCard.value() != this.value) return false;
        if (otherCard.suit() != this.suit) return false;
        return true;
    }

    @Override
    public int hashCode() {
//        return this.suit.getValue() * this.value;
        String concat = "" + this.suit.getValue() + this.value;
        return Integer.parseInt(concat);
    }

    @Override
    public String toString() {
        return this.value + " of " + this.suit;
    }
}
