package de.uniwue.gdp.pdd.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Standard52 implements Deck {

    private List<Card> deck; // Nachziehstapel
    private List<Card> trayStack; // Ablagestapel

    private Standard52() {
        this.deck = new ArrayList<>();
        this.trayStack = new ArrayList<>();
    }

    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    public void setTrayStack(List<Card> trayStack) {
        this.trayStack = trayStack;
    }

    @Override
    public Card take() {
        if (this.deck.isEmpty()) {
            if (this.trayStack.size() > 1) {
                Card cardToKeep = this.top();

                this.trayStack.remove(cardToKeep);

                this.deck = new ArrayList<>(this.trayStack);
                Collections.shuffle(this.deck);

                this.trayStack = new ArrayList<>();
                this.trayStack.add(cardToKeep);
            } else {
                throw new OutOfCardsException();
            }
        }
        Card card = this.deck.get(0);
        this.deck.remove(0);
        return card;
    }

    @Override
    public void play(Card card) {
        this.trayStack.add(card);
    }

    @Override
    public Card top() {
        if (this.trayStack.isEmpty()) {
            throw new OutOfCardsException();
        } else {
            return this.trayStack.get(trayStack.size() - 1);
        }
    }

    public static Deck singleDeck() {
        Standard52 standard52 = new Standard52();
        List<Card> trayStack = new ArrayList<>();
        List<Card> deck = new ArrayList<>();

        Suit[] suits = {Suit.DIAMONDS, Suit.CLUBS, Suit.HEARTS, Suit.SPADES};
        for (int i = 1; i <= 13; i++) {
            for (Suit suit : suits) {
                deck.add(new Card(suit, i));
            }
        }

        Card top = getRandomCard(deck);
        trayStack.add(top);
        deck.remove(top);

        Collections.shuffle(deck);

        standard52.setTrayStack(trayStack);
        standard52.setDeck(deck);

        return standard52;
    }

    public static Deck doubleDeck() {
        Standard52 standard52 = new Standard52();
        List<Card> trayStack = new ArrayList<>();
        List<Card> deck = new ArrayList<>();

        Suit[] suits = {Suit.DIAMONDS, Suit.CLUBS, Suit.HEARTS, Suit.SPADES};
        for (int i = 1; i <= 13; i++) {
            for (Suit suit : suits) {
                deck.add(new Card(suit, i));
                deck.add(new Card(suit, i));
            }
        }

        Card top = getRandomCard(deck);
        trayStack.add(top);
        deck.remove(top);

        Collections.shuffle(deck);

        standard52.setTrayStack(trayStack);
        standard52.setDeck(deck);

        return standard52;
    }

    private static Card getRandomCard(List<Card> cards) {
        Random rand = new Random();
        return cards.get(rand.nextInt(cards.size()));
    }
}
